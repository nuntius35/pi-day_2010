\documentclass[12pt,a4paper]{article}

\include{header}

\setlength{\oddsidemargin}{-6 mm}

\hypersetup{pdfinfo = {
  Title = {Quadratisch, praktisch, \ldots},
  Author = {Andreas Geyer-Schulz},
  Subject = Pi-Day 2010}
}

\begin{document}
\begin{center}
{\huge Quadratisch, praktisch, \ldots}

\vspace{5mm}
{\Large 14.~März 2010 -- {$\pi$-Day}}

\vspace{5mm}
{\large Andreas Geyer-Schulz}
\end{center}

In seinem hervorragenden Buch {\em \glqq Unglaublich einfach. Einfach unglaublich.\grqq} widmet sich Werner Gruber auch dem Bau von Papierfliegern \cite[Seite~167~ff.]{gruber}.
Die 18~Schritte der Anleitung des besonders raffinierten Fliegers \glqq Immamura Spezial\grqq\ beginnen mit folgenden Worten:

\begin{quote}
  \emph{\glqq [...] Jeder Anfänger kann ihn werfen -- mit einem wunderbaren Flug als Ergebnis.
  Man nehme ein \textbf{quadratisches} Blatt Papier.\grqq}
  [Hervorh. durch den Verf.]
\end{quote}

Leider sind quadratische Papierblätter gar nicht so leicht  aufzutreiben.
Die bei uns am häufigsten verwendeten Blätter entsprechen der DIN-Norm und weisen ein Seitenverhältnis von $\sqrt{2}:1$ auf\footnote{Das ist durchaus nützlich.
Wenn man ein solches Blatt entlang der längeren Seite in der Mitte teilt, dann ist das Seitenverhältnis der beiden neuen Blätter wieder $1:\frac{\sqrt{2}}{2} = \sqrt{2}:1$.}.
Glücklicherweise kann man sich relativ einfach behelfen.
Wenn man mit einem DIN-A4 Blatt startet, so kann man doch relativ leicht ein Quadrat davon abtrennen -- der dafür nötige Faltvorgang ist in Abbildung~\ref{dinsquare} gezeigt.
\begin{figure}[hb]
  \begin{center}
    \includegraphics[height = 50mm]{din2square.eps}
  \end{center}
  \caption{So erhält man ein Quadrat aus einem rechteckigen Papier.}
  \label{dinsquare}
\end{figure}

Aus dem Quadrat entsteht nun geschwind ein \glqq Immamura Spezial\grqq, viel spannender ist jedoch der rechteckige Rest.
Auch von ihm kann man wieder Quadrate abtrennen, in diesem Fall sind es $2$~Stück (Abbildung~\ref{dinsquare2}) und wieder bleibt etwas übrig.
Daraus entstehen nochmals 2 Quadrate (man probiere es aus!) und ein schon ziemlich kleiner Rest.

\begin{figure}[hb]
  \begin{center}
    \includegraphics[width = 150mm]{din2square2.eps}
  \end{center}
  \caption{So gehts weiter.}
  \label{dinsquare2}
\end{figure}

Dem interessierten Mathematiker drängen sich sofort zwei Fragen auf.
\begin{enumerate}
  \item \label{quest1}
    Geht das immer so weiter oder bleibt irgendwann nichts mehr übrig?
  \item \label{quest2}
    Wie viele neue Quadrate erhält man in jedem Schritt, gibt es dabei vielleicht ein Muster?
\end{enumerate}

Wir wollen diese beiden Fragen nun systematisch untersuchen.
Unsere Ausgangssituation soll ein rechteckiges
Blatt Papier mit dem Seitenverhältnis $x:1$ sein, dabei sei $x$ eine beliebige positive reelle Zahl.
Zunächst kümmern wir uns um Frage~\ref{quest2} und wollen einen Algorithmus entwerfen, der uns zu gegebenen $x$ angibt, wie viele Quadrate in jedem neuen Schritt entstehen.
Diese Anzahlen werden mit $[n_0;n_1,n_2,\ldots]$ bezeichnet, unter Umständen bricht diese Folge ab.
Das Seitenverhältnis des verbliebenen Reststückes nach dem $k$-ten Abschneiden von Quadraten wird mit $R_k = \frac{a_k}{b_k}$ bezeichnet, das heißt es handelt sich um ein Papierstück mit der Höhe $a_k$ und der Breite $b_k$.
Da die Höhe des neuen Restes die Breite des alten Restes war, bemerken wir $a_k = b_{k-1}$.
\begin{itemize}
  \item
    \textbf{(Der 0.~Schritt)}
    Am Anfang hat das ganze Papier (\glqq der nullte Rest\grqq) die Abmessungen $x$ zu $1$, in Kurzschreibweise
    \marginpar{
      \mbox{Bsp. DIN-A4} \\
      \vspace{-1mm}
      \mbox{$R_0 = \sqrt{2}$}}
    \begin{equation*}
      R_0 = \frac{x}{1}.
    \end{equation*}
    Die Anzahl der Quadrate, die man abschneiden kann, ist die größte ganze Zahl, die kleiner oder gleich $x$ ist, wir notieren also \marginpar{
      \vspace{2mm}
      \mbox{$n_0 = \lfloor \sqrt{2}\rfloor = 1$}}
    \begin{equation*}
      n_0 = \lfloor x\rfloor.
    \end{equation*}
  \item
    \textbf{(Der 1.~Schritt)}
    Dann bleibt folgender Rest übrig
    \marginpar{
      \vspace{5mm}
      \mbox{$R_1 = \frac{1}{\sqrt{2}-1}$}}
    \begin{equation*}
      R_1 = \frac{1}{x-\lfloor x\rfloor}
    \end{equation*}
    Von diesem können wir soviele Quadrate abschneiden, wie $x-\lfloor x\rfloor$ in $1$ hineinpasst, also
    \marginpar{
      \vspace{0mm}
      \mbox{$n_1 = \left\lfloor \frac{1}{\sqrt{2}-1}\right\rfloor = 2$}\\
      \mbox{$R_2 = \frac{\sqrt{2}-1}{1-2\cdot(\sqrt{2}-1)} = $}
      \mbox{$ = \frac{1}{\sqrt{2}-1} = R_1$}
      \mbox{$n_2 = \lfloor R_2\rfloor = 2 = n_1$}
      \mbox{$\vdots$}\\
      \mbox{$R_k = \frac{1}{\sqrt{2}-1}$}
      \mbox{$n_k = \lfloor R_2\rfloor = 2$}}
    \begin{equation*}
      n_1 = \left\lfloor \frac{1}{x-\lfloor x\rfloor}\right\rfloor
    \end{equation*}
  \item
    \textbf{(Der k.~Schritt)}
    Es sei uns der Rest $R_{k-1} = \frac{a_{k-1}}{b_{k-1}}$ bekannt, von diesem haben wir $n_{k-1} = \lfloor R_{k-1} \rfloor$ Quadrate abgeschnitten.
    Dann ergibt sich (falls $b_k = 0$ bricht das Verfahren ab)
    \begin{equation} \label{rrec}
      R_k
      = \frac{b_{k-1}}{a_{k-1}-n_{k-1}\cdot b_{k-1}}
      = \frac{a_k}{b_k}
    \end{equation}
    und
    \begin{equation*}
      n_k = \lfloor R_k \rfloor.
    \end{equation*}
\end{itemize}

Nach diesem Schema wird nun jeder positiven reellen Zahl $x$ auf eindeutige Weise eine
Folge $[n_0;n_1,n_2,\ldots]$ zugeordnet, die angibt, wie viele neue Quadrate in jedem Schritt abgeschnitten werden können.
Wer die Beispielrechnung am Rand verfolgt, sieht etwa, dass der Zahl $\sqrt{2}$ die Folge $[1;2,2,\ldots]$ zugeordnet wird, das heißt es entstehen in jedem Schritt, außer ganz am Anfang, genau 2 neue Quadrate.

Damit ist die \ref{quest2}.~Frage, im Wesentlichen beantwortet.
Wenn uns jemand eine Blatt mit dem Seitenverhältnis $x$ vorlegt, können wir ausrechnen, wie viele Quadrate sukzessive abzuschneiden sind.
Um die \ref{quest1}.~Frage zu beantworten (hört es irgendwann auf?) muss man sich überlegen, ob diese Folge $[n_0;n_1,n_2,\ldots]$ irgendetwas \glqq sinnvolles\grqq\ beschreibt.
In der Tat ist dies der Fall.

Es gilt (vgl. 1.~Schritt)
\begin{equation*}
  n_0 + \frac{1}{R_1}
  = \lfloor x\rfloor + \frac{1}{\frac{1}{x-\lfloor x\rfloor}}
  = \lfloor x\rfloor + x - \lfloor x\rfloor
  = x.                                                                                                                             \end{equation*}
Außerdem erhält man durch Nachrechnen für jedes $k\in\mathbb{N}$ unter Verwendung von Gleichung~{\color{blue}\eqref{rrec}}
\begin{equation}\label{rec}
  n_{k-1} + \frac{1}{\color{blue} R_k}
  = n_{k-1} + \frac{1}{\color{blue}\frac{b_{k-1}}{a_{k-1}-n_{k-1}\cdot b_{k-1}}}
  = n_{k-1} + \frac{a_{k-1}}{b_{k-1}} -n_{k+1}
  = \frac{a_{k-1}}{b_{k-1}} = R_{k-1}.
\end{equation}

Wenn wir Gleichung~{\color{blue}\eqref{rec}} nun wiederholt anwenden, so ergibt sich
\begin{equation} \label{confrac}
  \begin{split}
    x
    & = n_0+\cfrac{1}{R_1}
    = n_0+\cfrac{1}{n_1 + \cfrac{1}{{\color{blue} R_2}}}
    = n_0+\cfrac{1}{n_1 + \cfrac{1}{\color{blue} n_2 + \cfrac{1}{R_3}}} \\
    & = n_0+\cfrac{1}{n_1 + \cfrac{1}{n_2 + \cfrac{1}{n_3 + \cfrac{1}{R_4}}}} = \cdots .
  \end{split}
\end{equation}

Die Zahlenfolge $[n_0;n_1,n_2,\ldots]$ nennt man wegen \eqref{confrac} naheliegenderweise \textbf{Kettenbruchdarstellung} der Zahl $x$.
Wenn die Folge $[n_0;n_1,\ldots,n_r]$ abbricht, dann gilt
\begin{equation*}
  x = n_0 + \cfrac{1}{n_1 + \cdots + \cfrac{1}{n_r}},
\end{equation*}
also muss $x$ eine rationale Zahl gewesen sein.
Sei umgekehrt $x = \frac{a_0}{b_0} = R_0$ mit $a_0,b_0\in\mathbb{N}$ rational.
Wenn wir $a_k = b_{k-1}$ verwenden, dann erhalten wir durch Betrachtung der Nenner in Gleichung~\eqref{rrec}:
\begin{equation*}
  a_{k+1}
  = a_{k-1} - \left\lfloor \frac{a_{k-1}}{a_k}\right\rfloor a_k,
\end{equation*}
das heißt, $a_{k+1}$ ist der Rest bei Division von $a_{k-1}$ durch $a_k$, also gilt
\begin{equation*}
  0
  \leq a_{k+1}
  < a_k
\end{equation*}
für jedes $k\in\mathbb{N}$.
Damit ist $(a_k)$ eine fallende Folge nichtnegativer ganzer Zahlen, muss also irgendwann $0$ werden, dies bedeutet, dass nichts mehr vom Papier übrigbleibt.
Damit ist gezeigt, dass der Spaß genau dann aufhört, wenn das Seitenverhältnis rational war.

Wir geben noch kurz weitere Eigenschaften von Kettenbrüchen an, für Beweise verweisen wir auf die kompakte Darstellung in
\cite[Seite~222~ff.]{bundschuh}:
Jede Folge $[n_0;n_1,n_2,\ldots]$ konvergiert gegen eine reelle Zahl, umgekehrt besitzt jede reelle Zahl eine eindeutige Kettenbruchentwicklung $[n_0;n_1,n_2,\ldots]$, für rationalen Zahlen muss man dabei fordern, dass das letzte Glied in der Kettenbruchdarstellung nicht $1$ ist, denn sonst wäre etwa $\frac{3}{2} = 1 + \frac{1}{2} = 1 + \frac{1}{1+\frac{1}{1}}$.
Die Kettenbruchentwicklung von $x$ ist genau dann periodisch, wenn $x$ Lösung einer Gleichung 2.~Grades ist.
Als Beispiel haben wir bereits
\begin{equation*}
  \sqrt{2}
  = 1 + \cfrac{1}{2 + \cfrac{1}{2 + \cdots}}
  = [1;\overline{2}]
\end{equation*}
gesehen.

Zum Schluss noch eine nette Überlegung.
Ein \glqq besonders schönes\grqq\ Rechteck mit dem Seitenverhältnis $\phi$ habe die Eigenschaft, dass man in jedem Schritt genau ein Quadrat abtrennen kann.
Die Kettenbruchentwicklung von $\phi$ lautet dann
\begin{equation*}
  \phi
  = 1 + \cfrac{1}{1 + \cfrac{1}{1 + \cdots}}
  = 1 +\cfrac{1}{\phi}                                                                \end{equation*}
Hieraus ergibt sich die quadratische Gleichung
\begin{equation*}
  \phi^2 - \phi - 1 = 0,
\end{equation*}
die die positive Lösung $\phi = 1,618\ldots$ besitzt.
Diese Zahl ist als goldener Schnitt bekannt und das \glqq besonders schöne\grqq\ Rechteck ist ein goldenes Rechteck!

\begin{figure}[hb]
  \begin{center}
    \includegraphics[width = 150mm]{parthenongoldenratio.eps}
  \end{center}
  \caption{Goldene Rechtecke am Parthenon-Tempel.}
\end{figure}

Ich wünsche einen schönen \textbf{$\pi$-Day 2010!}
Einladungen, Beschwerden, Kritik sind an \href{mailto:gsa@posteo.de}{gsa@posteo.de} zu richten.

\begin{thebibliography}{10}
  \bibitem{gruber}
    {\sc Werner Gruber:}
    {\em Unglaublich einfach. Einfach unglaublich. Physik für jeden Tag.}
    \newblock {Ecowin Verlag , Salzburg, 2006.}
  \bibitem{bundschuh}
    {\sc Peter Bundschuh:}
    {\em Einführung in die Zahlentheorie.}
    \newblock {6.~Auf\-lage, Springer-Verlag, Berlin, Heidelberg, 2008.}
\end{thebibliography}

\end{document}
